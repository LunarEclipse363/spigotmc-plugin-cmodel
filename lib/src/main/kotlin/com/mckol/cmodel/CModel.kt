package com.mckol.cmodel

import com.mckol.cmodel.commands.CustomModelDataCommand
import dev.jorel.commandapi.CommandAPI
import dev.jorel.commandapi.CommandAPICommand
import dev.jorel.commandapi.arguments.IntegerArgument
import org.bukkit.plugin.java.JavaPlugin

class CModel : JavaPlugin() {
    override fun onLoad() {
        CommandAPI.onLoad(false)
    }

    override fun onEnable() {
        CommandAPI.onEnable(this)
        CommandAPICommand("custommodeldata")
                .withAliases("cmdata")
                .withPermission("cmodel.custommodeldata")
                .withSubcommand(
                        CommandAPICommand("get")
                                .executesPlayer(CustomModelDataCommand()::onGet)
                )
                .withSubcommand(
                        CommandAPICommand("set")
                                .executesPlayer(CustomModelDataCommand()::onSet)
                                .withArguments(IntegerArgument("value"))
                )
                .withSubcommand(
                        CommandAPICommand("clear")
                                .executesPlayer(CustomModelDataCommand()::onClear)
                )
                .register()
    }

    override fun onDisable() {}
}
