import kr.entree.spigradle.kotlin.spigot
import kr.entree.spigradle.kotlin.spigotmc
import com.github.jengelman.gradle.plugins.shadow.tasks.ConfigureShadowRelocation
import kr.entree.spigradle.kotlin.codemc

plugins {
    // Apply the org.jetbrains.kotlin.jvm Plugin to add support for Kotlin.
    id("org.jetbrains.kotlin.jvm") version "1.3.72"

    // Apply the java-library plugin for API and implementation separation.
    id("java-library")

    // Spigradle plugin for generating plugin.yml and simplifying build.gradle.kts
    id("kr.entree.spigradle") version "2.2.3"

    // Shadow libraries to make sure they don't interfere with other plugins or the server
    // Documentation: https://imperceptiblethoughts.com/shadow/
    id("com.github.johnrengelman.shadow") version "6.1.0"
}

version = "1.2.1"
val mcVersion = "1.16.5"
spigot {
    name = "CModel"
    authors = listOf("Mckol <mckol363@gmail.com>")
    apiVersion = "1.16"
    permissions {
        create("cmodel.*") {
            description = "Wildcard permission"
            defaults = "op"
            children = mapOf("cmodel.custommodeldata" to true)
        }
        create("cmodel.custommodeldata") {
            description = "Allows custommodeldata command"
            defaults = "false"
        }
    }
}

repositories {
    jcenter()
    spigotmc()
    maven("https://raw.githubusercontent.com/JorelAli/CommandAPI/mvn-repo/") // CommandsAPI repo
    codemc() // Required by CommandsAPI dependency NBT-API
}

dependencies {
    // Align versions of all Kotlin components
    implementation(platform(kotlin("bom")))

    // Use the Kotlin JDK 8 standard library.
    implementation(kotlin("stdlib-jdk8"))

    // Kotlin test stuff
    testImplementation(kotlin("test"))
    testImplementation(kotlin("test-junit"))

    // Spigot API
    compileOnly(spigot(mcVersion))

    // CommandsAPI
    implementation("dev.jorel:commandapi-shade:5.7")
}

// Shadow all runtime dependencies included in the jar
val relocateShadowJar by tasks.creating(ConfigureShadowRelocation::class) {
    target = tasks.shadowJar.get()
    prefix = "${project.group}.${project.name.toLowerCase()}.shadow"
}

tasks.shadowJar {
    dependsOn(relocateShadowJar)

    // Improve the name of the output jar
    archiveBaseName.set(spigot.name)
    archiveClassifier.set("")

    // Remove unused classes from the jar so that it's nice and small
    minimize()
}

tasks.build {
    // Make it so `gradle build` produces a shadowed jar
    dependsOn(tasks.shadowJar.get())
}
