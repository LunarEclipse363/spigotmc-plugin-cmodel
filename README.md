# CModel - The Spigot Plugin for chaniging CustomModelData of items
This plugin is made to do just this one thing. That's it.

## Usage:
- `(custommodeldata|cmdata) <subcommand> [args]` 
    - `cmdata get` - Shows the `CustomModelData` value of the currently held item.
    - `cmdata set <value>` - Sets the `CustomModelData` value of the currently held item.
    - `cmdata clear` - Clears the `CustomModelData` value of the currently held item.

## Permissions:
- `cmodel.custommodeldata` - Allows the usage of the plugin.

## License:
This project is licensed under the Affero GPL 3.0. 
